
# GIT commands

git clone git@gitlab.forge.hefr.ch:davidean.previte/BadgeRFID.git
cd BadgeRFID


# Projet de semestre 5 2018

Ce projet a pour but de nous familiariser avec la gestion de projet, en effet, les dates et les rendus sont définis par nous-même

Ce projet consiste a élaborer un système embarqué capable de lire les badges de l'école et optionnelllement authentifer le détenteur du badge pour qu'il puisse se connecter aux machines de la SmartFactory de Chimie.

Il faut trouver un système embarqué pas trop encombrant et plein de ressources pour une utilisation avec le protocole OPC UA déjà mis en place.

Il faut ensuite un lecteur RFID capable de lire les badges, et un client LDAP qui puisse se connecter au serveur de l'école pour lire les données du badges (optionnel).




