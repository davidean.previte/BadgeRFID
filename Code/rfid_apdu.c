#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>


#include "rfid.h"

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

char uidS[MAX_UID];

//open the reader with termios structure
int openSerial(const char *serial)
{
    int fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);

    if (fd == -1)
        printf("Error! in Opening RFID Reader \n");
    else
        printf("RFID Reader Opened Successfully \n");

    /**---------- Setting the Attributes of the serial port using termios structure --------- */

    struct termios SerialPortSettings; /* Create the structure                          */

    tcgetattr(fd, &SerialPortSettings); /* Get the current attributes of the Serial port */

    cfsetispeed(&SerialPortSettings, B115200); /* Set Read  Speed as 9600                       */
    cfsetospeed(&SerialPortSettings, B115200); /* Set Write Speed as 9600                       */

    SerialPortSettings.c_cflag &= ~PARENB; /* Disables the Parity Enable bit(PARENB),So No Parity   */
    SerialPortSettings.c_cflag &= ~CSTOPB; /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
    SerialPortSettings.c_cflag &= ~CSIZE;  /* Clears the mask for setting the data size             */
    SerialPortSettings.c_cflag |= CS8;     /* Set the data bits = 8                                 */

    SerialPortSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
    SerialPortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */

    SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);         /* Disable XON/XOFF flow control both i/p and o/p */
    SerialPortSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG); /* Non Cannonical mode                            */

    SerialPortSettings.c_oflag &= ~OPOST; /*No Output Processing*/

    if ((tcsetattr(fd, TCSANOW, &SerialPortSettings)) != 0) /* Set the attributes to the termios structure*/
        printf("ERROR ! in Setting attributes \n");
    else
        printf("BaudRate = 115200 \n  StopBits = 1 \n  Parity   = none\n");

    return fd;
}

uint16_t UpdateCRC(uint16_t CRC, byte Byte){
    Byte ^=(byte) CRC;
    Byte ^=(byte)(Byte<<4);
    return (uint16_t)(((Byte<<8) | (CRC>>8) ^ (Byte<<4) ^(Byte <<3)));
}


//read the uid and return it
char* readUID(int fd)
{
    unsigned nbBytes = 0;
    int bytes_to_read = 0;
    
    ioctl(fd, FIONREAD, &bytes_to_read);
    //The uid is only 7bytes or 4bytes, so if the bytes_to_read isn't one of this two value, doesn't to read)
    if (bytes_to_read != 0){// && (bytes_to_read == 15 || bytes_to_read == 9)) {
        //printf("Some data to read %d\n", bytes_to_read);
        while(1){
            char x;
            read(fd, &x, 1);
            //the reader send a LF for the last uid
            if (x == '\n') {
                break;
            }
            uidS[nbBytes++] = x;
            if (nbBytes  >= sizeof(uidS)) {
                printf("there is an error");
                return 0; // --> error!!!
            }
        }
    }
    uidS[nbBytes] = 0;
    return uidS;
}




void main(){
    openSerial("ttyS4");
    

}