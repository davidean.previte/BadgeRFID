//gcc -std=c99 -DUA_ARCHITECTURE_POSIX open62541.c server_rfid.c -o server -lpthread

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include "open62541.h"
#include "rfid.h"
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

//Define the sampling time for the sensor
#define SLEEP_TIME_MILLIS 50
//Define the ID of the node externally as it will be needed inside the thread
#define COUNTER_NODE_ID 20305

UA_Boolean running = true;

static void stopHandler(int sig);

void sendUID(char* uid);

//Thread used to monitorize the sensor 
void *mainSensor(void *ptr);

static void addUidVariable(UA_Server *server);

static void addCheckVariable(UA_Server *server);

int main(void);