#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

//Warning : must be bi enough !!
#define MAX_UID 50

//open the reader with termios structure
int openSerial(const char *serial);

//read the uid and return it
char* readUID(int fd);


