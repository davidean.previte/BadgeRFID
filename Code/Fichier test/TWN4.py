import serial
import codecs
import time

def int2bytes(i):
    hex_value = '{0:x}'.format(i)
    # make length of hex_value a multiple of two
    hex_value = '0' * (len(hex_value) % 2) + hex_value
    return codecs.decode(hex_value, 'hex_codec')

prot = {
    'enquiry_module': b'\x03\x12\x00\x15',
    'enquiry_module_return': b'\x02\x12\x14',
    'active_buzzer': b'\x02\x13\x15',
    'reqA': b'\x03\x26',
    'getUid' : b'\x25\x26\x00',
    'getUserInfo': b'\x85\x01\x78',
    'anticollA': b'\x04',
    'halt' : b'\xAA \x00 \x01 \x06 \x07 \xBB',
    'apdu': b'\xaa\x00\x08\x28\x00\x05\xff\xca\x00\x00\x00\xa9\xbb',
    'enquiry_cards_return': b'\x03\x02\x01\x06', # got valid card
    'enquiry_no_card_found': b'\xAA\x01\x03', # no card reachable or invalid
    'enquiry_all_cards': b'\x03\x02\x01\x05',
    'anticollision' : b'\x02\x03\x05\x00',
    'select_card' : b'\x02\x04\x06',
}



ser = serial.Serial('/dev/ttyS4', 115200, timeout=1)
print ("Connexion") 

while True :

    #ser.write(b'\x00\x06')
    #time.sleep(0.2)
    message = ser.read(20)
  
    print(message)


ser.close()
