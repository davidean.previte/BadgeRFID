import serial
import codecs
import time

from opcua import Server
from random import randint
import datetime

def readSerial():
    resp = ''
    resp =  ser.read()
    if(resp.encode('hex')==''):
        return ''
    message = resp

    while(resp.encode('hex') != ''):
        resp = ser.read()
        message = message + resp
    return message

def sendUID(uid):
    
    UID.set_value(uid)
    print uid
    TIME = datetime.datetime.now()
    Time.set_value(TIME)



ser = serial.Serial('/dev/ttyS4', 9600, timeout=1)
print ("Connexion") 

server = Server()

url = "opc.tcp://localhost:4840"

server.set_endpoint(url)

name = "OPCUA_RFID_SERVER"
addspace = server.register_namespace(name)

node = server.get_objects_node()

Param = node.add_object(addspace, "Parameters")

UID = Param.add_variable(addspace, "UID", 0)
Time = Param.add_variable(addspace, "Time", 0)

UID.set_writable()
Time.set_writable()

server.start()
print("Server started at {}".format(url))


while True :
   
    #time.sleep(0.2)
    uid = readSerial()
    if (uid == ''):
        #print "No card Detected"
        continue

    print("Card detected")
    sendUID(uid)


ser.close()
