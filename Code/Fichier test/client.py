from opcua import Client
import time

url = "opc.tcp://160.98.126.207:4840"

client = Client(url)
client.connect()

print("Client connected")

while True:
    Temp = client.get_node("ns=2;i=2")
    UID = Temp.get_value()
    print(UID)

    Time = client.get_node("ns=2;i=3")
    Date = Time.get_value()
    print(Date)

    time.sleep(1.5)