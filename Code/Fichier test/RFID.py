import serial
import codecs
import time

from opcua import Server
from random import randint
import datetime

def int2bytes(i):
    hex_value = '{0:x}'.format(i)
    # make length of hex_value a multiple of two
    hex_value = '0' * (len(hex_value) % 2) + hex_value
    return codecs.decode(hex_value, 'hex_codec')

prot = {
    'getUid' : b'\x25\x26\x00'
}

#permet de calculer le checksum
def crc(packet):
    iterator = iter(packet)
    hexalist = list()
    for i in iterator:
        i = i.encode('hex')
        hexalist.append(i)
    sum = 0
    for i in hexalist:
        #convertir hex en int
        sum = sum ^ int(i, 16)
    return int2bytes(sum)

#permet de créer une liste de nombre hexa avec l'UID lu
def getHexlist(msg):
    iterator = iter(msg)
    hexalist = list()
    for i in iterator:
        i = i.encode('hex')
        hexalist.append(i)
    return hexalist

#permet de créer le paquet à envoyer au lecteur pour lire la carte
def packet(command):
    length = int2bytes(len(command))
    packet = b'\x00'+length+command
    check = crc(packet)

    return b'\xaa'+packet+check+b'\xbb'

#permet de lire les données sur le lecteur
def readSerial():
    resp = ''
    resp =  ser.read()
    if(resp.encode('hex')==''):
        return ''
    message = resp

    while(resp.encode('hex') != 'bb'):
        resp = ser.read()
        message = message + resp
    return message

#permet d'envoyer les informations aux client OPC UA
def getUID(msgList):
    
    length = len(msgList)-1
    if (length>6):
        print(msgList[5:length-1], length)
        UID.set_value(msgList[5:length-1])
        TIME = datetime.datetime.now()
        Time.set_value(TIME)


ser = serial.Serial('/dev/ttyS3', 115200, timeout=1)
print ("Connexion") 

server = Server()

url = "opc.tcp://localhost:4840"

server.set_endpoint(url)

name = "OPCUA_SIMULATION_SERVER"
addspace = server.register_namespace(name)

node = server.get_objects_node()

Param = node.add_object(addspace, "Parameters")

UID = Param.add_variable(addspace, "UID", 0)
Time = Param.add_variable(addspace, "Time", 0)

UID.set_writable()
Time.set_writable()

server.start()
print("Server started at {}".format(url))


while True :
    send = packet(prot['getUid'])

    ser.write(send)
    time.sleep(0.2)
    message = readSerial()
    if (message == ''):
        print "Error while reading"
        continue

    msgList = getHexlist(message)
    length = len(msgList) - 1

    if (msgList[length-2] == '83'):
        print "Error, no card"
        continue

    print("Card detected")
    getUID(msgList)


ser.close()
