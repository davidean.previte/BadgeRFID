import serial
import codecs
import time

from opcua import Server
from random import randint
import datetime

def int2bytes(i):
    hex_value = '{0:x}'.format(i)
    # make length of hex_value a multiple of two
    hex_value = '0' * (len(hex_value) % 2) + hex_value
    return codecs.decode(hex_value, 'hex_codec')

prot = {
    'enquiry_module': b'\x03\x12\x00\x15',
    'enquiry_module_return': b'\x02\x12\x14',
    'active_buzzer': b'\x02\x13\x15',
    'reqA': b'\x03\x26',
    'getUid' : b'\x25\x26\x00',
    'getUserInfo': b'\x85\x01\x78',
    'anticollA': b'\x04',
    'halt' : b'\xAA \x00 \x01 \x06 \x07 \xBB',
    'apdu': b'\xaa\x00\x08\x28\x00\x05\xff\xca\x00\x00\x00\xa9\xbb',
    'enquiry_cards_return': b'\x03\x02\x01\x06', # got valid card
    'enquiry_no_card_found': b'\xAA\x01\x03', # no card reachable or invalid
    'enquiry_all_cards': b'\x03\x02\x01\x05',
    'anticollision' : b'\x02\x03\x05\x00',
    'select_card' : b'\x02\x04\x06',
}

def crc(packet):
    iterator = iter(packet)
    hexalist = list()
    for i in iterator:
        i = i.encode('hex')
        hexalist.append(i)
    sum = 0
    for i in hexalist:
        #convertir hex en int
        sum = sum ^ int(i, 16)
    return int2bytes(sum)

def getHexlist(msg):
    iterator = iter(msg)
    hexalist = list()
    for i in iterator:
        i = i.encode('hex')
        hexalist.append(i)
    return hexalist

def packet(command):
    length = int2bytes(len(command))
    packet = b'\x00'+length+command
    check = crc(packet)

    return b'\xaa'+packet+check+b'\xbb'

def readSerial():
    resp = ''
    resp =  ser.read()
    if(resp.encode('hex')==''):
        return ''
    message = resp

    while(resp.encode('hex') != 'bb'):
        resp = ser.read()
        message = message + resp
    return message

def getUID(msgList):
    
    length = len(msgList)-1
    if (length>6):
        print(msgList[5:length-1], length)
        UID.set_value(msgList[5:length-1])
        TIME = datetime.datetime.now()
        Time.set_value(TIME)



ser = serial.Serial('/dev/ttyS3', 115200, timeout=1)
print ("Connexion") 

server = Server()

url = "opc.tcp://localhost:4840"

server.set_endpoint(url)

name = "OPCUA_SIMULATION_SERVER"
addspace = server.register_namespace(name)

node = server.get_objects_node()

Param = node.add_object(addspace, "Parameters")

UID = Param.add_variable(addspace, "UID", 0)
Time = Param.add_variable(addspace, "Time", 0)

UID.set_writable()
Time.set_writable()

server.start()
print("Server started at {}".format(url))


while True :
    send = packet(prot['getUid'])

    ser.write(send)
    time.sleep(0.2)
    message = readSerial()
    if (message == ''):
        print "Error while reading"
        continue

    msgList = getHexlist(message)
    length = len(msgList) - 1

    if (msgList[length-2] == '83'):
        print "Error, no card"
        continue

    print("Card detected")
    getUID(msgList)


ser.close()
