#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <asm/ioctl.h>
#include <linux/ioctl.h>

int main()
{
    //const char getSnr[] = {0xaa, 0x00, 0x03, 0x25, 0x26, 0x00, 0x00, 0xbb};
    int fd = open("/dev/ttyS5", O_RDWR | O_NOCTTY | O_NDELAY);

    if (fd == -1)
        printf("\n  Error! in Opening RFID Reader  ");
    else
        printf("\n RFID Reader Opened Successfully ");

    // /**---------- Setting the Attributes of the serial port using termios structure --------- */

    struct termios SerialPortSettings; /* Create the structure                          */

    tcgetattr(fd, &SerialPortSettings); /* Get the current attributes of the Serial port */

    cfsetispeed(&SerialPortSettings, B9600); /* Set Read  Speed as 9600                       */
    cfsetospeed(&SerialPortSettings, B9600); /* Set Write Speed as 9600                       */

    SerialPortSettings.c_cflag &= ~PARENB; /* Disables the Parity Enable bit(PARENB),So No Parity   */
    SerialPortSettings.c_cflag &= ~CSTOPB; /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
    SerialPortSettings.c_cflag &= ~CSIZE;  /* Clears the mask for setting the data size             */
    SerialPortSettings.c_cflag |= CS8;     /* Set the data bits = 8                                 */

    SerialPortSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
    SerialPortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */

    SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);         /* Disable XON/XOFF flow control both i/p and o/p */
    SerialPortSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG); /* Non Cannonical mode                            */

    SerialPortSettings.c_oflag &= ~OPOST; /*No Output Processing*/

    if ((tcsetattr(fd, TCSANOW, &SerialPortSettings)) != 0) /* Set the attributes to the termios structure*/
        printf("\n  ERROR ! in Setting attributes");
    else
        printf("\n  BaudRate = 115200 \n  StopBits = 1 \n  Parity   = none\n");

    /*------------------------------- Write data to serial port -----------------------------*/

    //unsigned char write_buffer[20]; /* Buffer containing characters to write into port	     */
    int bytes_written = 0;          /* Value for storing the number of bytes written to the port */

    //bytes_written = write(fd, &getSnr, sizeof(getSnr)); /* use write() to send data to port                                            */
    /* "fd"                   - file descriptor pointing to the opened serial port */
    /*	"write_buffer"         - address of the buffer containing data	            */
    /* "sizeof(write_buffer)" - No of bytes to write                               */
    // printf("\n  %s written to ttyS3", &getSnr[3]);
    //printf("\n  %d Bytes written to ttyS3", bytes_written);
    //printf("\n +----------------------------------+\n\n");

    int nbBytes = 0;
    unsigned char UID[20];

    while (1)
    {
        uint8_t x;
        (ioctl (fd, FIONREAD, &bytes_written));
        if(bytes_written > 0) printf("%d\n", bytes_written);
        bytes_written = read(fd, &x, 1);
        if (bytes_written != 1)
        {
            if (nbBytes > 0)
            {
                printf("%s\n", UID);
                memset(&UID[0], 0, sizeof(UID));
                nbBytes = 0;
            }
        }
        else
        {
            UID[nbBytes] = ((int)x) & 0xFF;
            nbBytes += 1;
        }
    }
    close(fd); /* Close the Serial port */

    return 0;
}