#include "rfid.h"

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

unsigned char* uid;
int nbBytes = 0;

//open the reader with termios structure
int openSerial(const char *serial)
{
    int fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);

    if (fd == -1)
        printf("Error! in Opening RFID Reader \n");
    else
        printf("RFID Reader Opened Successfully \n");

    /**---------- Setting the Attributes of the serial port using termios structure --------- */

    struct termios SerialPortSettings; /* Create the structure                          */

    tcgetattr(fd, &SerialPortSettings); /* Get the current attributes of the Serial port */

    cfsetispeed(&SerialPortSettings, B9600); /* Set Read  Speed as 9600                       */
    cfsetospeed(&SerialPortSettings, B9600); /* Set Write Speed as 9600                       */

    SerialPortSettings.c_cflag &= ~PARENB; /* Disables the Parity Enable bit(PARENB),So No Parity   */
    SerialPortSettings.c_cflag &= ~CSTOPB; /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
    SerialPortSettings.c_cflag &= ~CSIZE;  /* Clears the mask for setting the data size             */
    SerialPortSettings.c_cflag |= CS8;     /* Set the data bits = 8                                 */

    SerialPortSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
    SerialPortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */

    SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);         /* Disable XON/XOFF flow control both i/p and o/p */
    SerialPortSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG); /* Non Cannonical mode                            */

    SerialPortSettings.c_oflag &= ~OPOST; /*No Output Processing*/

    if ((tcsetattr(fd, TCSANOW, &SerialPortSettings)) != 0) /* Set the attributes to the termios structure*/
        printf("ERROR ! in Setting attributes \n");
    else
        printf("BaudRate = 9600 \n  StopBits = 1 \n  Parity   = none\n");

    return fd;
}

//read the uid and return it
unsigned char* readUID(int fd)
{
    uint8_t x;
    read(fd, &x, 1);
     printf("UID is %d\n", x);
    uid[nbBytes] = ((int)x) & 0xFF;
    printf("UID is %s\n", uid);
    nbBytes += 1;
    return uid;
}

//this method must be called after sending the uid to the client OPC
unsigned char* clearUID(void)
{
    printf("The UID that was read is : %s\n", uid);
    memset(&uid[0], 0, MAX_UID);
    nbBytes = 0;
    return uid;
    
}

