#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define MAX_UID 20

//open the reader with termios structure
int openSerial(const char *serial);

//read the uid and return it
unsigned char* readUID(int fd);

//this method must be called after sending the uid to the client OPC
unsigned char* clearUID(void);

