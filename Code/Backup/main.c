#include "rfid.h"

unsigned char* uid;
int bytes_to_read = 0;

int main(void){
    int fd = openSerial("/dev/ttyS4");
    uid = malloc(MAX_UID);

    while (1){
        //if there is some data to read, we read them and save them
        (ioctl(fd, FIONREAD, &bytes_to_read));
        if (bytes_to_read > 0){
            uid = readUID(fd);

            //if we read the last bytes, we must send the data to the opc client
            (ioctl(fd, FIONREAD, &bytes_to_read));
            if (bytes_to_read == 0){
                //sendUID(uid);
                /*uint8_t cha;
                read(fd, &cha, 1);

                printf("caracter spéciale %d", cha);*/
                uid = clearUID();
            }
        }
    }
    free(uid);
}
