# Install script for directory: /mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/bin/libopen62541.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/open62541/open62541Targets.cmake")
    file(DIFFERENT EXPORT_FILE_CHANGED FILES
         "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/open62541/open62541Targets.cmake"
         "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/CMakeFiles/Export/lib/cmake/open62541/open62541Targets.cmake")
    if(EXPORT_FILE_CHANGED)
      file(GLOB OLD_CONFIG_FILES "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/open62541/open62541Targets-*.cmake")
      if(OLD_CONFIG_FILES)
        message(STATUS "Old export file \"$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/cmake/open62541/open62541Targets.cmake\" will be replaced.  Removing files [${OLD_CONFIG_FILES}].")
        file(REMOVE ${OLD_CONFIG_FILES})
      endif()
    endif()
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/open62541" TYPE FILE FILES "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/CMakeFiles/Export/lib/cmake/open62541/open62541Targets.cmake")
  if("${CMAKE_INSTALL_CONFIG_NAME}" MATCHES "^([Dd][Ee][Bb][Uu][Gg])$")
    file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/open62541" TYPE FILE FILES "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/CMakeFiles/Export/lib/cmake/open62541/open62541Targets-debug.cmake")
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/open62541" TYPE FILE FILES
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/cmake/open62541Config.cmake"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/open62541ConfigVersion.cmake"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/open62541" TYPE FILE RENAME "open62541Macros.cmake" FILES "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/tools/cmake/macros_public.cmake")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/open62541.pc")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/open62541/tools" TYPE DIRECTORY FILES
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/tools/certs"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/tools/nodeset_compiler"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/tools/schema"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/deps/ua-nodeset"
    USE_SOURCE_PERMISSIONS)
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/open62541/tools" TYPE FILE FILES
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/tools/generate_datatypes.py"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/tools/generate_nodeid_header.py"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/tools/generate_statuscode_descriptions.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/open62541" TYPE FILE FILES
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/deps/open62541_queue.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/deps/ziptree.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/deps/pcg_basic.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/deps/libc_time.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/deps/base64.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/ua_util_internal.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/ua_types_encoding_binary.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/ua_types_generated_encoding_binary.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/ua_transport_generated.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/ua_transport_generated_handling.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/ua_transport_generated_encoding_binary.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/ua_connection_internal.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/ua_securechannel.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/server/ua_discovery_manager.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/ua_workqueue.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/ua_timer.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/server/ua_session.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/server/ua_subscription.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/server/ua_session_manager.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/server/ua_securechannel_manager.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/pubsub/ua_pubsub_networkmessage.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/pubsub/ua_pubsub.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/pubsub/ua_pubsub_manager.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/pubsub/ua_pubsub_ns0.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/server/ua_server_internal.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/server/ua_services.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/ua_namespace0.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/src/client/ua_client_internal.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/ua_config.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/arch/ua_architecture_base.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/arch/posix/ua_architecture.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/deps/ms_stdint.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/arch/ua_architecture_definitions.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/ua_statuscodes.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/ua_nodeids.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_constants.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_types.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/ua_types_generated.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/src_generated/ua_types_generated_handling.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_util.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_server.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_plugin_historydatabase.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/historydata/ua_plugin_history_data_backend.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/historydata/ua_plugin_history_data_gathering.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_plugin_log.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_plugin_network.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_plugin_access_control.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_plugin_pki.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_plugin_securitypolicy.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_server_pubsub.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_plugin_pubsub.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_plugin_nodestore.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_server_config.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_client_config.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_client.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_client_highlevel.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_client_subscriptions.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/include/ua_client_highlevel_async.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/ua_accesscontrol_default.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/historydata/ua_historydatabackend_memory.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/historydata/ua_historydatagathering_default.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/historydata/ua_historydatabase_default.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/ua_pki_certificate.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/ua_log_stdout.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/ua_nodestore_default.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/ua_config_default.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/securityPolicies/ua_securitypolicies.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/historydata/ua_historydatabackend_memory.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/historydata/ua_historydatagathering_default.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/plugins/historydata/ua_historydatabase_default.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/arch/ua_network_tcp.h"
    "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/arch/ua_architecture_functions.h"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/arch/cmake_install.cmake")
  include("/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/doc/cmake_install.cmake")
  include("/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/tools/packaging/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/mnt/c/Users/Davide/OneDrive/Documents/HEIA-FR/Cours/3/ProjetRFID/Code/open62541/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
