/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Project:		HEIA-FR / Project 5
 *
 * Abstract:	Badge RFID connected to the iIoT
 *	
 * Purpose:		OPC UA server that send the UID read from the TWN4 to the client OPC UA
 *
 * Author:		Davide Previte / HEIA-FR
 * Date:		15.01.2019
 * 
 * 
 * Site where to find how to change the variable in the opc ua server
 * https://medium.com/gradiant-talks/implementing-an-opc-ua-server-using-open62541-77aeddd99370
 * 
 * Site where to find how to change the hostname 
 * https://github.com/open62541/open62541/issues/535
 * 
 */


#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include "open62541.h"
#include "server_rfid.h"
#include "rfid.h"
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

//Define the sampling time for the reader
#define SLEEP_TIME_MILLIS 50

//Define the ID of the node externally as it will be needed inside the thread
#define UID_NODE_ID 20305
#define CHECK_NODE_ID 21305

UA_String myUID;
UA_Int32 myInteger;
UA_UInt32 myChecked;

UA_Server *server;

//UA_String_init(&myUID); /* _init zeroes out the entire memory of the datatype */

static void stopHandler(int sig)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Received ctrl-c");
    running = false;
}

void sendUID(char *uid)
{
    //printf("Card detected: %s\n", uid);
    UA_Variant value;
    myUID.length = strlen(uid);
    myUID.data = (UA_Byte *)uid;
    UA_Variant_setScalarCopy(&value, &myUID, &UA_TYPES[UA_TYPES_STRING]);
    UA_Server_writeValue(server, UA_NODEID_NUMERIC(1, UID_NODE_ID), value);
}

//Thread used to monitor the RFID reader
void *mainReader(void *ptr)
{
    server = ptr;
    int utime = SLEEP_TIME_MILLIS * 10;
    char uid[MAX_UID];
    char uid_old[MAX_UID];
    char msg[MAX_UID];
    int nbCheck = 0;

    int fd = openSerial("/dev/ttyACM0");

    while (running == 1)
    {
        char *uid = readUID(fd);

        //si UID identique, incremente nbCheck
        if (strcmp(uid, uid_old) == 0)
        {
            //printf("Deux fois la meme carte\n");
            nbCheck= nbCheck+1;
            snprintf(msg, MAX_UID, "%s ; %d", uid,    nbCheck);
            sendUID(msg);

        }else{

            //printf("New card %s\n", uid);
            nbCheck = 1;
            snprintf(msg, MAX_UID, "%s ; %d", uid,    nbCheck);
            sendUID(msg);
            strcpy(uid_old, uid);
        }

        usleep(utime);
    }
}

static void addUidVariable(UA_Server *server)
{
    UA_NodeId uidNodeId = UA_NODEID_NUMERIC(1, UID_NODE_ID);
    UA_QualifiedName uidName = UA_QUALIFIEDNAME(1, "UID");
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    attr.description = UA_LOCALIZEDTEXT("en_US", "UID number");
    attr.displayName = UA_LOCALIZEDTEXT("en_US", "UID");
    attr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    //UA_String myUID;
    UA_Variant_setScalarCopy(&attr.value, &myUID, &UA_TYPES[UA_TYPES_STRING]);
    UA_Server_addVariableNode(server, uidNodeId,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                              UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                              uidName, UA_NODEID_NULL, attr, NULL, NULL);
}

/*
static void addCheckVariable(UA_Server *server)
{

    UA_NodeId NodeId = UA_NODEID_NUMERIC(1, CHECK_NODE_ID);
    UA_QualifiedName Name = UA_QUALIFIEDNAME(1, "Checked number of UID");
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    attr.description = UA_LOCALIZEDTEXT("en_US", "Checked number");
    attr.displayName = UA_LOCALIZEDTEXT("en_US", "Checked");
    attr.dataType = UA_TYPES[UA_TYPES_INT32].typeId;
    //UA_String myUID;
    UA_Variant_setScalarCopy(&attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
    UA_Server_addVariableNode(server, NodeId,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                              UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                              Name, UA_NODEID_NULL, attr, NULL, NULL);
     /* Define the attribute of the myInteger variable node 
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    myInteger = 0;
    UA_Variant_setScalar(&attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
    attr.description = UA_LOCALIZEDTEXT("en_US", "Checked number");
    attr.displayName = UA_LOCALIZEDTEXT("en_US", "Checked");
    attr.dataType = UA_TYPES[UA_TYPES_INT32].typeId;
    attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    Add the variable node to the information model 
    UA_NodeId myIntegerNodeId = UA_NODEID_STRING(1, "Checked number of UID");
    UA_QualifiedName myIntegerName = UA_QUALIFIEDNAME(1, "Number of time");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, CHECK_NODE_ID);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(server, myIntegerNodeId, parentNodeId,
                              parentReferenceNodeId, myIntegerName,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), attr, NULL, NULL);
}
*/

int main(void)
{
    int ret;
    pthread_t threadReader;
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);
    UA_ServerConfig *config = UA_ServerConfig_new_default();
    UA_Server *server = UA_Server_new(config);

    addUidVariable(server);
    //addCheckVariable(server);

    //Launch the thread. The OPC-UA server is passed as parameter as the value of the node needs to be updated.
    if (pthread_create(&threadReader, NULL, mainReader, server))
    {
        fprintf(stderr, "Error - pthread_create(): %d\n", ret);
        exit(EXIT_FAILURE);
    }
    UA_StatusCode retval = UA_Server_run(server, &running);
    UA_Server_delete(server);
    UA_ServerConfig_delete(config);
    return (int)retval;
}