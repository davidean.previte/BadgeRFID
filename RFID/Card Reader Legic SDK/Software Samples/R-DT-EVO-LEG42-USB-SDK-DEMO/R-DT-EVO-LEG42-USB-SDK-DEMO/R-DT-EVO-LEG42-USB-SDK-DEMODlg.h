#pragma once
#include "afxwin.h"
#include "HexEdit.h"


class CRDTEVOLEG42USBSDKDEMODlg : public CDialog
{
public:
	CRDTEVOLEG42USBSDKDEMODlg(CWnd* pParent = NULL);	// standard constructor

	enum { IDD = IDD_RDTEVOLEG42USBSDKDEMO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	HICON m_hIcon;

	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBnClickedButtonLed();
	DECLARE_MESSAGE_MAP()
private:
	void Scan();

private:
	CComboBox m_ctrlPort;
	CButton m_ctrlLeds;
public:
	afx_msg void OnBnClickedButtonBuzzer();
	afx_msg void OnBnClickedButtonReaderInfo();
	CButton m_ctrlBuzzer;
	CEdit m_ctrlReaderType;
	CEdit m_ctrlHardwareRev;
	CEdit m_ctrlPower;
	CEdit m_ctrlBootLoader;
	CEdit m_ctrlFirmware;
	CEdit m_ctrlCryptMode;
	afx_msg void OnBnClickedButtonCard();
	CComboBox m_ctrlCard;
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedButtonWrite();
	CHexEdit m_ctrlData;
};
