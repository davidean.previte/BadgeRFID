#include "stdafx.h"
#include "R-DT-EVO-LEG42-USB-SDK-DEMO.h"
#include "R-DT-EVO-LEG42-USB-SDK-DEMODlg.h"
#include "R-DT-EVO-LEG42-USB-SDK.h"

#include "SetupApi.h"
#include "SdkString.h"
#include "Util.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()





CRDTEVOLEG42USBSDKDEMODlg::CRDTEVOLEG42USBSDKDEMODlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRDTEVOLEG42USBSDKDEMODlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CRDTEVOLEG42USBSDKDEMODlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_COM, m_ctrlPort);
	DDX_Control(pDX, IDC_CHECK_LEDS, m_ctrlLeds);
	DDX_Control(pDX, IDC_CHECK_BUZZER, m_ctrlBuzzer);
	DDX_Control(pDX, IDC_EDIT_READER_TYPE, m_ctrlReaderType);
	DDX_Control(pDX, IDC_EDIT_HARDWARE_REV, m_ctrlHardwareRev);
	DDX_Control(pDX, IDC_EDIT_POWER, m_ctrlPower);
	DDX_Control(pDX, IDC_EDIT_BOOT_LOADER, m_ctrlBootLoader);
	DDX_Control(pDX, IDC_EDIT_FIRMWARE, m_ctrlFirmware);
	DDX_Control(pDX, IDC_EDIT_CRYPT_MODE, m_ctrlCryptMode);
	DDX_Control(pDX, IDC_COMBO_CARD, m_ctrlCard);
	DDX_Control(pDX, IDC_EDIT_DATA, m_ctrlData);
}

BEGIN_MESSAGE_MAP(CRDTEVOLEG42USBSDKDEMODlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_LED, &CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedButtonLed)
	ON_BN_CLICKED(IDC_BUTTON_BUZZER, &CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedButtonBuzzer)
	ON_BN_CLICKED(IDC_BUTTON_READER_INFO, &CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedButtonReaderInfo)
	ON_BN_CLICKED(IDC_BUTTON_CARD, &CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedButtonCard)
	ON_BN_CLICKED(IDOK, &CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_WRITE, &CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedButtonWrite)
END_MESSAGE_MAP()


BOOL CRDTEVOLEG42USBSDKDEMODlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	Scan();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CRDTEVOLEG42USBSDKDEMODlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CRDTEVOLEG42USBSDKDEMODlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

HCURSOR CRDTEVOLEG42USBSDKDEMODlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CRDTEVOLEG42USBSDKDEMODlg::Scan()
{
	BOOL bFound = FALSE;
	DWORD dwPos = 0x00, dwCom = -1;

	while((dwCom = GetAvailableCom(dwPos)) != -1)
	{
		CString strCom = _T("");
		strCom.Format(_T("COM%d"), dwCom);
		m_ctrlPort.SetItemData(m_ctrlPort.AddString(strCom), dwCom);

		dwPos++;
	}

	m_ctrlPort.SetCurSel(0);
}

void CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedButtonLed()
{
	UINT nPort = (UINT)m_ctrlPort.GetItemData(m_ctrlPort.GetCurSel());	
	SDKHANDLE hReader = OpenReader(nPort);
	if(hReader != NULL)
	{
		BYTE bLeds = m_ctrlLeds.GetCheck();
		SetReaderCaps(hReader, ERD_LEDS, &bLeds, sizeof(BYTE));
		CloseReader(hReader);
	}
}

void CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedButtonBuzzer()
{
	UINT nPort = (UINT)m_ctrlPort.GetItemData(m_ctrlPort.GetCurSel());	
	SDKHANDLE hReader = OpenReader(nPort);
	if(hReader != NULL)
	{
		BYTE bBuzzer = m_ctrlBuzzer.GetCheck();
		SetReaderCaps(hReader, ERD_BUZZER, &bBuzzer, sizeof(BYTE));
		CloseReader(hReader);
	}
}

void CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedButtonReaderInfo()
{
	UINT nPort = (UINT)m_ctrlPort.GetItemData(m_ctrlPort.GetCurSel());	
	SDKHANDLE hReader = OpenReader(nPort);
	if(hReader != NULL)
	{
		char szData[MAX_PATH]; DWORD dwSize = MAX_PATH;
		GetReaderCaps(hReader, ERD_READER_TYPE, (BYTE*)szData, dwSize);

		USES_CONVERSION;
		m_ctrlReaderType.SetWindowText(A2T(szData));

		dwSize = MAX_PATH;
		GetReaderCaps(hReader, ERD_HARDAWARE_REV, (BYTE*)szData, dwSize);
		m_ctrlHardwareRev.SetWindowText(A2T(szData));

		dwSize = MAX_PATH;
		GetReaderCaps(hReader, ERD_POWER, (BYTE*)szData, dwSize);
		m_ctrlPower.SetWindowText(A2T(szData));

		dwSize = MAX_PATH;
		GetReaderCaps(hReader, ERD_BOOT_LOADER, (BYTE*)szData, dwSize);
		m_ctrlBootLoader.SetWindowText(A2T(szData));

		dwSize = MAX_PATH;
		GetReaderCaps(hReader, ERD_FIRMWARE, (BYTE*)szData, dwSize);
		m_ctrlFirmware.SetWindowText(A2T(szData));

		dwSize = MAX_PATH;
		GetReaderCaps(hReader, ERD_CRYPT_MODE, (BYTE*)szData, dwSize);
		m_ctrlCryptMode.SetWindowText(A2T(szData));

		CloseReader(hReader);
	}
}

void CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedButtonCard()
{
	m_ctrlCard.ResetContent();
	UINT nPort = (UINT)m_ctrlPort.GetItemData(m_ctrlPort.GetCurSel());	
	SDKHANDLE hReader = OpenReader(nPort);
	if(hReader != NULL)
	{
		BYTE pData[MAX_PATH], bCardType = ECARD_LEGIC; DWORD dwData = MAX_PATH;
		pData[0x00] = bCardType;
		if(GetReaderCaps(hReader, ERD_CARD, (BYTE*)pData, dwData) == ER_OK)
		{
			BYTE* pDataNav = pData;
			// display all cards
			UINT nNrCards = *(DWORD*)pDataNav; pDataNav += sizeof(DWORD);
			for(UINT nCount = 0x00; nCount < nNrCards; nCount++)
			{
				SdkString strRet = CUtil::ByteToString(pDataNav + 0x01, pDataNav[0x00]);
				m_ctrlCard.SetItemData(m_ctrlCard.AddString(strRet.c_str()), nCount);
			}
			m_ctrlCard.SetCurSel(0x00);
		}
		CloseReader(hReader);
	}
}

void CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedOk()
{
	UINT nPort = (UINT)m_ctrlPort.GetItemData(m_ctrlPort.GetCurSel());	
	SDKHANDLE hReader = OpenReader(nPort);
	if(hReader != NULL)
	{
		DWORD pInfoData[0x03];
		pInfoData[0x00] = ECARD_LEGIC;
		if(m_ctrlCard.GetCurSel() != LB_ERR)
		{
			// select card
			pInfoData[0x01] = (DWORD)m_ctrlCard.GetItemData(m_ctrlCard.GetCurSel());
			pInfoData[0x02] = 0x00;	// no speed
			if(SetReaderCaps(hReader, ERD_CARD, (BYTE*)pInfoData, sizeof(pInfoData)) == ER_OK)
			{
				// select file
				BYTE pDataFile[MAX_PATH];
				ZeroMemory(pDataFile, MAX_PATH);
				DWORD* pDataFileInfo	= (DWORD*)pDataFile;
				pDataFileInfo[0x00]		= 0x02;		// segment
				pDataFileInfo[0x01]		= FILE_DATA_SEG;
				pDataFileInfo[0x02]		= 0x00; //(DWORD)strlen("SEG") + 0x01;
				// memcpy_s(pDataFile + 3 * sizeof(DWORD) + 1, MAX_PATH, "SEG", strlen("SEG"));

				if(SetReaderCaps(hReader, ERD_SELECT_FILE, pDataFile, 3 * sizeof(DWORD)) == ER_OK)
				{
					BYTE pData[MAX_PATH]; DWORD dwData = 20;
					ReadData(hReader, 0x00, pData, dwData);
					m_ctrlData.SetData(pData, dwData);
					m_ctrlData.RedrawWindow();
				}
			}
		}
		CloseReader(hReader);
	}
}

void CRDTEVOLEG42USBSDKDEMODlg::OnBnClickedButtonWrite()
{
	UINT nPort = (UINT)m_ctrlPort.GetItemData(m_ctrlPort.GetCurSel());	
	SDKHANDLE hReader = OpenReader(nPort);
	if(hReader != NULL)
	{
		DWORD pInfoData[0x03];
		pInfoData[0x00] = ECARD_LEGIC;
		if(m_ctrlCard.GetCurSel() != LB_ERR)
		{
			// select card
			pInfoData[0x01] = (DWORD)m_ctrlCard.GetItemData(m_ctrlCard.GetCurSel());
			pInfoData[0x02] = 0x00;	// no speed
			if(SetReaderCaps(hReader, ERD_CARD, (BYTE*)pInfoData, sizeof(pInfoData)) == ER_OK)
			{
				// select file
				BYTE pDataFile[MAX_PATH];
				ZeroMemory(pDataFile, MAX_PATH);
				DWORD* pDataFileInfo	= (DWORD*)pDataFile;
				pDataFileInfo[0x00]		= 0x02;		// segment
				pDataFileInfo[0x01]		= FILE_DATA_SEG;
				pDataFileInfo[0x02]		= 0x00; // (DWORD)strlen("SEG") + 0x01;
				// memcpy_s(pDataFile + 3 * sizeof(DWORD) + 1, MAX_PATH, "SEG", strlen("SEG"));

				if(SetReaderCaps(hReader, ERD_SELECT_FILE, pDataFile, 3 * sizeof(DWORD)) == ER_OK)
				{
					BYTE pData[MAX_PATH]; DWORD dwData = 4;
					WriteData(hReader, 0x00, pData, dwData);
					m_ctrlData.SetData(pData, dwData);
					m_ctrlData.RedrawWindow();
				}
			}
		}
		CloseReader(hReader);
	}
}
