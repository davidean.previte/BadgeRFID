//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by R-DT-EVO-LEG42-USB-SDK-DEMO.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_RDTEVOLEG42USBSDKDEMO_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDC_STATIC_COM                  1002
#define IDC_COMBO1                      1003
#define IDC_COMBO_COM                   1003
#define IDC_STATIC_READER_INFO          1004
#define IDC_STATIC_READER_FEATURE       1005
#define IDC_CHECK_LEDS                  1006
#define IDC_BUTTON_LED                  1007
#define IDC_CHECK_BUZZER                1008
#define IDC_BUTTON_BUZZER               1009
#define IDC_STATIC_READER_TYPE          1010
#define IDC_EDIT_READER_TYPE            1011
#define IDC_BUTTON_READER_INFO          1012
#define IDC_STATIC_HARDWARE_REV         1013
#define IDC_EDIT_HARDWARE_REV           1014
#define IDC_STATIC_POWER                1015
#define IDC_EDIT_POWER                  1016
#define IDC_STATIC_BOOTLOADER           1017
#define IDC_EDIT_BOOT_LOADER            1018
#define IDC_STATIC_FIRMWARE             1019
#define IDC_EDIT_FIRMWARE               1020
#define IDC_STATIC_CRYPT_MODE           1021
#define IDC_EDIT_CRYPT_MODE             1022
#define IDC_STATIC_CARD                 1023
#define IDC_COMBO_CARD                  1024
#define IDC_BUTTON_CARD                 1025
#define IDC_STATIC_READ_WRITE           1026
#define IDC_EDIT7                       1027
#define IDC_EDIT_DATA                   1027
#define IDC_BUTTON_WRITE                1029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
