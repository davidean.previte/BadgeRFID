#pragma once

#include "Error.h"
#include "tchar.h"
#include "SdkString.h"


typedef struct _ERROR_NAME
{
	TCHAR*	szError;
	EERROR	eError;
}ERROR_NAME, *PERROR_NAME;

class CUtil
{
public:
	CUtil();
	virtual ~CUtil();
	static UINT CalcCRC16(BYTE* pData);
	static EERROR GetError(BYTE bError);
	static EERROR SetDataStr(CHAR* szSrc, BYTE* pDest, DWORD& dwDestLen);
	static EERROR SetDataByte(BYTE* pSrc, DWORD dwSrc, BYTE* pDest, DWORD& dwDestLen);
	static TCHAR* GetError(EERROR eError);
	static SdkString ByteToString(BYTE* pData, UINT nSize);
};

#define START_ERROR()	DWORD dwTime = GetTickCount()

#define END_ERROR(X)		CString strError = _T("");	\
							strError.Format(_T("%s: %s ! Exec time: %d (ms)"), X, CUtil::GetError(eRet), GetTickCount() - dwTime);	\
							m_ctrlStatus.SetWindowText(strError)