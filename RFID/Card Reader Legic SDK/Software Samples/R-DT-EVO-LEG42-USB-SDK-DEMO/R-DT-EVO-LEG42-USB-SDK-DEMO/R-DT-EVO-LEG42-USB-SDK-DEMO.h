#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


class CRDTEVOLEG42USBSDKDEMOApp : public CWinApp
{
public:
	CRDTEVOLEG42USBSDKDEMOApp();
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

extern CRDTEVOLEG42USBSDKDEMOApp theApp;