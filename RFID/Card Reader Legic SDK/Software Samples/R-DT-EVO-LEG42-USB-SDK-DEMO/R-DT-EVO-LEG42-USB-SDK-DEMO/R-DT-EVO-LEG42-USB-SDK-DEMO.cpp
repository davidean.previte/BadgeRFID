#include "stdafx.h"
#include "R-DT-EVO-LEG42-USB-SDK-DEMO.h"
#include "R-DT-EVO-LEG42-USB-SDK-DEMODlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


BEGIN_MESSAGE_MAP(CRDTEVOLEG42USBSDKDEMOApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


CRDTEVOLEG42USBSDKDEMOApp::CRDTEVOLEG42USBSDKDEMOApp()
{
}


CRDTEVOLEG42USBSDKDEMOApp theApp;


BOOL CRDTEVOLEG42USBSDKDEMOApp::InitInstance()
{

	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);

	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CRDTEVOLEG42USBSDKDEMODlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{

	}
	else if (nResponse == IDCANCEL)
	{

	}

	return FALSE;
}
