#pragma once

enum CTRLMODE
{	
	CTRL_NONE,
	CTRL_TEXT,
	CTRL_HEX_HIGH,
	CTRL_HEX_LOW
};

class CHexEdit : public CEdit
{
private:
	BYTE*	m_pData;
	UINT	m_nLength;
	UINT	m_nCurAddress;
	UINT	m_nTopIndex;

	UINT	m_nOffHex;
	UINT	m_nOffAscii;
	UINT	m_nLineHeight;
	UINT	m_nNullWidth;
	UINT	m_nLpp;
	BOOL	m_bHalfPage;
	BOOL	m_bUpdate;
	CPoint	m_pctEditPos;
	CFont	m_clsFont;
	CTRLMODE	m_ctrlHex;


	CPoint CalcPos(int x, int y);
	void CreateEditCaret();
	void RepositionCaret(int p);
	void Move(int x, int y);
	void CreateAddressCaret();
	void UpdateScrollbars();
	void DrawHex(CDC* pDc, CRect rcDC, UINT nHeight);
	void DrawAscii(CDC* pDc, CRect rcDC, UINT nHeight);

public:
	CHexEdit();
	~CHexEdit();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);

	virtual void SetData(BYTE* pData, UINT nLength);
	virtual UINT GetData(BYTE* pData, UINT nLength);
	DECLARE_MESSAGE_MAP()
};