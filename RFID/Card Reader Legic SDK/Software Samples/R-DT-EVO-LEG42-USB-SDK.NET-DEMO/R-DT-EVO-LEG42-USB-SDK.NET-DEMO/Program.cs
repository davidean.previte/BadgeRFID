using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace R_DT_EVO_LEG42_USB_SDK.NET_DEMO
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }
    }
}