namespace R_DT_EVO_LEG42_USB_SDK.NET_DEMO
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.tabMainControl = new System.Windows.Forms.TabControl();
            this.tabPageInfo = new System.Windows.Forms.TabPage();
            this.textInfoCryptMode = new System.Windows.Forms.TextBox();
            this.labelInfoCrypt = new System.Windows.Forms.Label();
            this.textInfoFirmware = new System.Windows.Forms.TextBox();
            this.labelInfoFirmware = new System.Windows.Forms.Label();
            this.textInfoBootLoader = new System.Windows.Forms.TextBox();
            this.labelInfoBootLoader = new System.Windows.Forms.Label();
            this.labelInfoPower = new System.Windows.Forms.Label();
            this.textInfoPower = new System.Windows.Forms.TextBox();
            this.textInfoHardwareRev = new System.Windows.Forms.TextBox();
            this.labelInfoHardwareRev = new System.Windows.Forms.Label();
            this.textInfoReaderType = new System.Windows.Forms.TextBox();
            this.labelInfoReaderType = new System.Windows.Forms.Label();
            this.buttonInfo = new System.Windows.Forms.Button();
            this.tabPageScan = new System.Windows.Forms.TabPage();
            this.buttonScan = new System.Windows.Forms.Button();
            this.listViewScan = new System.Windows.Forms.ListView();
            this.columnCardType = new System.Windows.Forms.ColumnHeader();
            this.columnCardId = new System.Windows.Forms.ColumnHeader();
            this.imageListScan = new System.Windows.Forms.ImageList(this.components);
            this.tabPageReadWrite = new System.Windows.Forms.TabPage();
            this.comboRWBoxData = new System.Windows.Forms.ComboBox();
            this.buttonRWWrite = new System.Windows.Forms.Button();
            this.buttonRWRead = new System.Windows.Forms.Button();
            this.textRWData = new System.Windows.Forms.TextBox();
            this.buttonRWCard = new System.Windows.Forms.Button();
            this.comboRWCard = new System.Windows.Forms.ComboBox();
            this.labelRWCard = new System.Windows.Forms.Label();
            this.labelCom = new System.Windows.Forms.Label();
            this.comboCom = new System.Windows.Forms.ComboBox();
            this.tabPageFeature = new System.Windows.Forms.TabPage();
            this.checkFeatureLeds = new System.Windows.Forms.CheckBox();
            this.buttonFeatureLeds = new System.Windows.Forms.Button();
            this.checkFeatureBuzzer = new System.Windows.Forms.CheckBox();
            this.buttonFeatureBuzzer = new System.Windows.Forms.Button();
            this.tabMainControl.SuspendLayout();
            this.tabPageInfo.SuspendLayout();
            this.tabPageScan.SuspendLayout();
            this.tabPageReadWrite.SuspendLayout();
            this.tabPageFeature.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabMainControl
            // 
            this.tabMainControl.Controls.Add(this.tabPageInfo);
            this.tabMainControl.Controls.Add(this.tabPageScan);
            this.tabMainControl.Controls.Add(this.tabPageReadWrite);
            this.tabMainControl.Controls.Add(this.tabPageFeature);
            this.tabMainControl.Location = new System.Drawing.Point(2, 50);
            this.tabMainControl.Name = "tabMainControl";
            this.tabMainControl.SelectedIndex = 0;
            this.tabMainControl.Size = new System.Drawing.Size(279, 243);
            this.tabMainControl.TabIndex = 8;
            // 
            // tabPageInfo
            // 
            this.tabPageInfo.Controls.Add(this.textInfoCryptMode);
            this.tabPageInfo.Controls.Add(this.labelInfoCrypt);
            this.tabPageInfo.Controls.Add(this.textInfoFirmware);
            this.tabPageInfo.Controls.Add(this.labelInfoFirmware);
            this.tabPageInfo.Controls.Add(this.textInfoBootLoader);
            this.tabPageInfo.Controls.Add(this.labelInfoBootLoader);
            this.tabPageInfo.Controls.Add(this.labelInfoPower);
            this.tabPageInfo.Controls.Add(this.textInfoPower);
            this.tabPageInfo.Controls.Add(this.textInfoHardwareRev);
            this.tabPageInfo.Controls.Add(this.labelInfoHardwareRev);
            this.tabPageInfo.Controls.Add(this.textInfoReaderType);
            this.tabPageInfo.Controls.Add(this.labelInfoReaderType);
            this.tabPageInfo.Controls.Add(this.buttonInfo);
            this.tabPageInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPageInfo.Name = "tabPageInfo";
            this.tabPageInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageInfo.Size = new System.Drawing.Size(271, 217);
            this.tabPageInfo.TabIndex = 0;
            this.tabPageInfo.Text = "Info";
            this.tabPageInfo.UseVisualStyleBackColor = true;
            // 
            // textInfoCryptMode
            // 
            this.textInfoCryptMode.Location = new System.Drawing.Point(104, 136);
            this.textInfoCryptMode.Name = "textInfoCryptMode";
            this.textInfoCryptMode.Size = new System.Drawing.Size(161, 20);
            this.textInfoCryptMode.TabIndex = 12;
            // 
            // labelInfoCrypt
            // 
            this.labelInfoCrypt.AutoSize = true;
            this.labelInfoCrypt.Location = new System.Drawing.Point(6, 143);
            this.labelInfoCrypt.Name = "labelInfoCrypt";
            this.labelInfoCrypt.Size = new System.Drawing.Size(34, 13);
            this.labelInfoCrypt.TabIndex = 11;
            this.labelInfoCrypt.Text = "Crypt:";
            // 
            // textInfoFirmware
            // 
            this.textInfoFirmware.Location = new System.Drawing.Point(104, 110);
            this.textInfoFirmware.Name = "textInfoFirmware";
            this.textInfoFirmware.Size = new System.Drawing.Size(161, 20);
            this.textInfoFirmware.TabIndex = 10;
            // 
            // labelInfoFirmware
            // 
            this.labelInfoFirmware.AutoSize = true;
            this.labelInfoFirmware.Location = new System.Drawing.Point(6, 117);
            this.labelInfoFirmware.Name = "labelInfoFirmware";
            this.labelInfoFirmware.Size = new System.Drawing.Size(52, 13);
            this.labelInfoFirmware.TabIndex = 9;
            this.labelInfoFirmware.Text = "Firmware:";
            // 
            // textInfoBootLoader
            // 
            this.textInfoBootLoader.Location = new System.Drawing.Point(104, 84);
            this.textInfoBootLoader.Name = "textInfoBootLoader";
            this.textInfoBootLoader.Size = new System.Drawing.Size(161, 20);
            this.textInfoBootLoader.TabIndex = 8;
            // 
            // labelInfoBootLoader
            // 
            this.labelInfoBootLoader.AutoSize = true;
            this.labelInfoBootLoader.Location = new System.Drawing.Point(6, 91);
            this.labelInfoBootLoader.Name = "labelInfoBootLoader";
            this.labelInfoBootLoader.Size = new System.Drawing.Size(64, 13);
            this.labelInfoBootLoader.TabIndex = 7;
            this.labelInfoBootLoader.Text = "Boot loader:";
            // 
            // labelInfoPower
            // 
            this.labelInfoPower.AutoSize = true;
            this.labelInfoPower.Location = new System.Drawing.Point(6, 65);
            this.labelInfoPower.Name = "labelInfoPower";
            this.labelInfoPower.Size = new System.Drawing.Size(40, 13);
            this.labelInfoPower.TabIndex = 6;
            this.labelInfoPower.Text = "Power:";
            // 
            // textInfoPower
            // 
            this.textInfoPower.Location = new System.Drawing.Point(104, 58);
            this.textInfoPower.Name = "textInfoPower";
            this.textInfoPower.Size = new System.Drawing.Size(161, 20);
            this.textInfoPower.TabIndex = 5;
            // 
            // textInfoHardwareRev
            // 
            this.textInfoHardwareRev.Location = new System.Drawing.Point(104, 32);
            this.textInfoHardwareRev.Name = "textInfoHardwareRev";
            this.textInfoHardwareRev.Size = new System.Drawing.Size(161, 20);
            this.textInfoHardwareRev.TabIndex = 4;
            // 
            // labelInfoHardwareRev
            // 
            this.labelInfoHardwareRev.AutoSize = true;
            this.labelInfoHardwareRev.Location = new System.Drawing.Point(6, 39);
            this.labelInfoHardwareRev.Name = "labelInfoHardwareRev";
            this.labelInfoHardwareRev.Size = new System.Drawing.Size(77, 13);
            this.labelInfoHardwareRev.TabIndex = 3;
            this.labelInfoHardwareRev.Text = "Hardware rev.:";
            // 
            // textInfoReaderType
            // 
            this.textInfoReaderType.Location = new System.Drawing.Point(104, 6);
            this.textInfoReaderType.Name = "textInfoReaderType";
            this.textInfoReaderType.Size = new System.Drawing.Size(161, 20);
            this.textInfoReaderType.TabIndex = 2;
            // 
            // labelInfoReaderType
            // 
            this.labelInfoReaderType.AutoSize = true;
            this.labelInfoReaderType.Location = new System.Drawing.Point(6, 13);
            this.labelInfoReaderType.Name = "labelInfoReaderType";
            this.labelInfoReaderType.Size = new System.Drawing.Size(68, 13);
            this.labelInfoReaderType.TabIndex = 1;
            this.labelInfoReaderType.Text = "Reader type:";
            // 
            // buttonInfo
            // 
            this.buttonInfo.Location = new System.Drawing.Point(193, 193);
            this.buttonInfo.Name = "buttonInfo";
            this.buttonInfo.Size = new System.Drawing.Size(75, 23);
            this.buttonInfo.TabIndex = 0;
            this.buttonInfo.Text = "Info";
            this.buttonInfo.UseVisualStyleBackColor = true;
            this.buttonInfo.Click += new System.EventHandler(this.buttonInfo_Click);
            // 
            // tabPageScan
            // 
            this.tabPageScan.Controls.Add(this.buttonScan);
            this.tabPageScan.Controls.Add(this.listViewScan);
            this.tabPageScan.Location = new System.Drawing.Point(4, 22);
            this.tabPageScan.Name = "tabPageScan";
            this.tabPageScan.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageScan.Size = new System.Drawing.Size(271, 217);
            this.tabPageScan.TabIndex = 1;
            this.tabPageScan.Text = "Scan";
            this.tabPageScan.UseVisualStyleBackColor = true;
            // 
            // buttonScan
            // 
            this.buttonScan.Location = new System.Drawing.Point(190, 193);
            this.buttonScan.Name = "buttonScan";
            this.buttonScan.Size = new System.Drawing.Size(75, 23);
            this.buttonScan.TabIndex = 1;
            this.buttonScan.Text = "Scan";
            this.buttonScan.UseVisualStyleBackColor = true;
            this.buttonScan.Click += new System.EventHandler(this.buttonScan_Click);
            // 
            // listViewScan
            // 
            this.listViewScan.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnCardType,
            this.columnCardId});
            this.listViewScan.LargeImageList = this.imageListScan;
            this.listViewScan.Location = new System.Drawing.Point(6, 6);
            this.listViewScan.Name = "listViewScan";
            this.listViewScan.Size = new System.Drawing.Size(259, 161);
            this.listViewScan.SmallImageList = this.imageListScan;
            this.listViewScan.TabIndex = 0;
            this.listViewScan.UseCompatibleStateImageBehavior = false;
            this.listViewScan.View = System.Windows.Forms.View.Details;
            // 
            // columnCardType
            // 
            this.columnCardType.Text = "Card type";
            this.columnCardType.Width = 90;
            // 
            // columnCardId
            // 
            this.columnCardId.Text = "Card ID";
            this.columnCardId.Width = 121;
            // 
            // imageListScan
            // 
            this.imageListScan.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListScan.ImageStream")));
            this.imageListScan.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListScan.Images.SetKeyName(0, "card.ICO");
            // 
            // tabPageReadWrite
            // 
            this.tabPageReadWrite.Controls.Add(this.comboRWBoxData);
            this.tabPageReadWrite.Controls.Add(this.buttonRWWrite);
            this.tabPageReadWrite.Controls.Add(this.buttonRWRead);
            this.tabPageReadWrite.Controls.Add(this.textRWData);
            this.tabPageReadWrite.Controls.Add(this.buttonRWCard);
            this.tabPageReadWrite.Controls.Add(this.comboRWCard);
            this.tabPageReadWrite.Controls.Add(this.labelRWCard);
            this.tabPageReadWrite.Location = new System.Drawing.Point(4, 22);
            this.tabPageReadWrite.Name = "tabPageReadWrite";
            this.tabPageReadWrite.Size = new System.Drawing.Size(271, 217);
            this.tabPageReadWrite.TabIndex = 2;
            this.tabPageReadWrite.Text = "Read/Write";
            this.tabPageReadWrite.UseVisualStyleBackColor = true;
            this.tabPageReadWrite.Enter += new System.EventHandler(this.tabPageReadWrite_Enter);
            // 
            // comboRWBoxData
            // 
            this.comboRWBoxData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRWBoxData.FormattingEnabled = true;
            this.comboRWBoxData.Location = new System.Drawing.Point(191, 132);
            this.comboRWBoxData.Name = "comboRWBoxData";
            this.comboRWBoxData.Size = new System.Drawing.Size(61, 21);
            this.comboRWBoxData.TabIndex = 6;
            // 
            // buttonRWWrite
            // 
            this.buttonRWWrite.Location = new System.Drawing.Point(90, 130);
            this.buttonRWWrite.Name = "buttonRWWrite";
            this.buttonRWWrite.Size = new System.Drawing.Size(75, 23);
            this.buttonRWWrite.TabIndex = 5;
            this.buttonRWWrite.Text = "Write";
            this.buttonRWWrite.UseVisualStyleBackColor = true;
            this.buttonRWWrite.Click += new System.EventHandler(this.buttonRWWrite_Click);
            // 
            // buttonRWRead
            // 
            this.buttonRWRead.Location = new System.Drawing.Point(9, 130);
            this.buttonRWRead.Name = "buttonRWRead";
            this.buttonRWRead.Size = new System.Drawing.Size(75, 23);
            this.buttonRWRead.TabIndex = 4;
            this.buttonRWRead.Text = "Read";
            this.buttonRWRead.UseVisualStyleBackColor = true;
            this.buttonRWRead.Click += new System.EventHandler(this.buttonRWRead_Click);
            // 
            // textRWData
            // 
            this.textRWData.Location = new System.Drawing.Point(9, 104);
            this.textRWData.Name = "textRWData";
            this.textRWData.ReadOnly = true;
            this.textRWData.Size = new System.Drawing.Size(243, 20);
            this.textRWData.TabIndex = 3;
            // 
            // buttonRWCard
            // 
            this.buttonRWCard.Location = new System.Drawing.Point(177, 35);
            this.buttonRWCard.Name = "buttonRWCard";
            this.buttonRWCard.Size = new System.Drawing.Size(75, 23);
            this.buttonRWCard.TabIndex = 2;
            this.buttonRWCard.Text = "Get";
            this.buttonRWCard.UseVisualStyleBackColor = true;
            this.buttonRWCard.Click += new System.EventHandler(this.buttonRWCard_Click);
            // 
            // comboRWCard
            // 
            this.comboRWCard.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRWCard.FormattingEnabled = true;
            this.comboRWCard.Location = new System.Drawing.Point(75, 8);
            this.comboRWCard.Name = "comboRWCard";
            this.comboRWCard.Size = new System.Drawing.Size(177, 21);
            this.comboRWCard.TabIndex = 1;
            // 
            // labelRWCard
            // 
            this.labelRWCard.AutoSize = true;
            this.labelRWCard.Location = new System.Drawing.Point(6, 16);
            this.labelRWCard.Name = "labelRWCard";
            this.labelRWCard.Size = new System.Drawing.Size(32, 13);
            this.labelRWCard.TabIndex = 0;
            this.labelRWCard.Text = "Card:";
            // 
            // labelCom
            // 
            this.labelCom.AutoSize = true;
            this.labelCom.Location = new System.Drawing.Point(-1, 18);
            this.labelCom.Name = "labelCom";
            this.labelCom.Size = new System.Drawing.Size(29, 13);
            this.labelCom.TabIndex = 9;
            this.labelCom.Text = "Port:";
            // 
            // comboCom
            // 
            this.comboCom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCom.FormattingEnabled = true;
            this.comboCom.Location = new System.Drawing.Point(36, 12);
            this.comboCom.Name = "comboCom";
            this.comboCom.Size = new System.Drawing.Size(78, 21);
            this.comboCom.TabIndex = 10;
            // 
            // tabPageFeature
            // 
            this.tabPageFeature.Controls.Add(this.buttonFeatureBuzzer);
            this.tabPageFeature.Controls.Add(this.checkFeatureBuzzer);
            this.tabPageFeature.Controls.Add(this.buttonFeatureLeds);
            this.tabPageFeature.Controls.Add(this.checkFeatureLeds);
            this.tabPageFeature.Location = new System.Drawing.Point(4, 22);
            this.tabPageFeature.Name = "tabPageFeature";
            this.tabPageFeature.Size = new System.Drawing.Size(271, 217);
            this.tabPageFeature.TabIndex = 3;
            this.tabPageFeature.Text = "Feature";
            this.tabPageFeature.UseVisualStyleBackColor = true;
            // 
            // checkFeatureLeds
            // 
            this.checkFeatureLeds.AutoSize = true;
            this.checkFeatureLeds.Location = new System.Drawing.Point(6, 16);
            this.checkFeatureLeds.Name = "checkFeatureLeds";
            this.checkFeatureLeds.Size = new System.Drawing.Size(91, 17);
            this.checkFeatureLeds.TabIndex = 0;
            this.checkFeatureLeds.Text = "Leds (On/Off)";
            this.checkFeatureLeds.UseVisualStyleBackColor = true;
            // 
            // buttonFeatureLeds
            // 
            this.buttonFeatureLeds.Location = new System.Drawing.Point(160, 10);
            this.buttonFeatureLeds.Name = "buttonFeatureLeds";
            this.buttonFeatureLeds.Size = new System.Drawing.Size(75, 23);
            this.buttonFeatureLeds.TabIndex = 1;
            this.buttonFeatureLeds.Text = "Leds";
            this.buttonFeatureLeds.UseVisualStyleBackColor = true;
            this.buttonFeatureLeds.Click += new System.EventHandler(this.buttonFeatureLeds_Click);
            // 
            // checkFeatureBuzzer
            // 
            this.checkFeatureBuzzer.AutoSize = true;
            this.checkFeatureBuzzer.Location = new System.Drawing.Point(6, 55);
            this.checkFeatureBuzzer.Name = "checkFeatureBuzzer";
            this.checkFeatureBuzzer.Size = new System.Drawing.Size(100, 17);
            this.checkFeatureBuzzer.TabIndex = 2;
            this.checkFeatureBuzzer.Text = "Buzzer (On/Off)";
            this.checkFeatureBuzzer.UseVisualStyleBackColor = true;
            // 
            // buttonFeatureBuzzer
            // 
            this.buttonFeatureBuzzer.Location = new System.Drawing.Point(160, 49);
            this.buttonFeatureBuzzer.Name = "buttonFeatureBuzzer";
            this.buttonFeatureBuzzer.Size = new System.Drawing.Size(75, 23);
            this.buttonFeatureBuzzer.TabIndex = 3;
            this.buttonFeatureBuzzer.Text = "Buzzer";
            this.buttonFeatureBuzzer.UseVisualStyleBackColor = true;
            this.buttonFeatureBuzzer.Click += new System.EventHandler(this.buttonFeatureBuzzer_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 305);
            this.Controls.Add(this.comboCom);
            this.Controls.Add(this.labelCom);
            this.Controls.Add(this.tabMainControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "Main ";
            this.Load += new System.EventHandler(this.Main_Load);
            this.tabMainControl.ResumeLayout(false);
            this.tabPageInfo.ResumeLayout(false);
            this.tabPageInfo.PerformLayout();
            this.tabPageScan.ResumeLayout(false);
            this.tabPageReadWrite.ResumeLayout(false);
            this.tabPageReadWrite.PerformLayout();
            this.tabPageFeature.ResumeLayout(false);
            this.tabPageFeature.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabMainControl;
        private System.Windows.Forms.TabPage tabPageInfo;
        private System.Windows.Forms.TabPage tabPageScan;
        private System.Windows.Forms.Label labelInfoReaderType;
        private System.Windows.Forms.Button buttonInfo;
        private System.Windows.Forms.TextBox textInfoReaderType;
        private System.Windows.Forms.TextBox textInfoHardwareRev;
        private System.Windows.Forms.Label labelInfoHardwareRev;
        private System.Windows.Forms.Label labelInfoPower;
        private System.Windows.Forms.TextBox textInfoPower;
        private System.Windows.Forms.TextBox textInfoBootLoader;
        private System.Windows.Forms.Label labelInfoBootLoader;
        private System.Windows.Forms.Label labelInfoFirmware;
        private System.Windows.Forms.TextBox textInfoFirmware;
        private System.Windows.Forms.TextBox textInfoCryptMode;
        private System.Windows.Forms.Label labelInfoCrypt;
        private System.Windows.Forms.ListView listViewScan;
        private System.Windows.Forms.ImageList imageListScan;
        private System.Windows.Forms.Button buttonScan;
        private System.Windows.Forms.ColumnHeader columnCardType;
        private System.Windows.Forms.ColumnHeader columnCardId;
        private System.Windows.Forms.TabPage tabPageReadWrite;
        private System.Windows.Forms.Button buttonRWCard;
        private System.Windows.Forms.ComboBox comboRWCard;
        private System.Windows.Forms.Label labelRWCard;
        private System.Windows.Forms.Button buttonRWWrite;
        private System.Windows.Forms.Button buttonRWRead;
        private System.Windows.Forms.TextBox textRWData;
        private System.Windows.Forms.ComboBox comboRWBoxData;
        private System.Windows.Forms.Label labelCom;
        private System.Windows.Forms.ComboBox comboCom;
        private System.Windows.Forms.TabPage tabPageFeature;
        private System.Windows.Forms.Button buttonFeatureLeds;
        private System.Windows.Forms.CheckBox checkFeatureLeds;
        private System.Windows.Forms.Button buttonFeatureBuzzer;
        private System.Windows.Forms.CheckBox checkFeatureBuzzer;

    }
}

