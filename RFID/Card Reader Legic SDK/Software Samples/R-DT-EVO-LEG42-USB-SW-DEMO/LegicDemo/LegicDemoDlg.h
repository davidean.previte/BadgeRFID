#pragma once
#include "afxwin.h"

#include "LegicProtocol.h"
#include "afxcmn.h"
#include "HexEdit.h"

typedef struct _SCAN_CARD
{
	BYTE	eCardType;
	TCHAR*	szMsg;
}GENTYPE_TO_STRING, *PSCAN_TYPE;

union CARD_SAVE_INFO
{
	struct
	{
		BYTE bCardPos;
		BYTE bCardSubtype;
		BYTE bCardType;
		BYTE bUnknown;
	}CardInfo;
	DWORD dwCardInfo;
};//CARD_SAVE_INFO, *PCARD_SAVE_INFO;

class CLegicDemoDlg : public CDialog
{
private:
	CFont			m_clsFont;
	CLegicProtocol	m_clsLegicProt;
	CImageList		m_lstImage;

private:
	void Scan();
	void InsertColumn(UINT nCol, TCHAR* szData, UINT nMaxLen);
	void InsertItem(UINT nIntem, UINT nSubItem, TCHAR* szString, UINT nImage);
	void EnableReadWrite(BOOL bState = TRUE);
	void EnableGetInfoScan(BOOL bState = TRUE);
	void EnableConnect(BOOL bState = TRUE);
	void CleanCardItems();
	EERROR ReadLegicCard();
	EERROR ReadUltralightCard();

public:
	CLegicDemoDlg(CWnd* pParent = NULL);	// standard constructor
	enum { IDD = IDD_LEGICDEMO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

protected:
	CComboBox m_ctrlPort;
	CEdit m_ctrlReaderType;
	CEdit m_ctrlHardwareRev;
	CEdit m_ctrlPower;
	CEdit m_ctrlBootLoader;
	CEdit m_ctrlFirmware;
	CEdit m_ctrlCryptMode;
	CEdit m_ctrlStatus;

	CButton m_btnGetInfo;
	CButton m_btnScan;
	CButton m_btnConnect;
	CButton m_btnDisconnect;
	CButton m_btnRead;
	CButton m_btnWrite;

	CListCtrl m_ctrlCards;
	CHexEdit m_ctrlData;

protected:
	afx_msg void OnBnClickedBtnReaderInfo();
	afx_msg void OnBnClickedBtnRead();
	afx_msg void OnBnClickedBtnWrite();
	afx_msg void OnBnClickedBtnConnect();
	afx_msg void OnBnClickedBtnDisconnect();
	afx_msg void OnBnClickedBtnScanCards();
	afx_msg void OnClose();
	afx_msg void OnLvnItemchangedListCards(NMHDR *pNMHDR, LRESULT *pResult);
};
