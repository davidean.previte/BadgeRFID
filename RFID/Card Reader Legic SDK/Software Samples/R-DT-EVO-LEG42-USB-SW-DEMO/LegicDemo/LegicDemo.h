// LegicDemo.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CLegicDemoApp:
// See LegicDemo.cpp for the implementation of this class
//

class CLegicDemoApp : public CWinApp
{
public:
	CLegicDemoApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CLegicDemoApp theApp;