//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by LegicDemo.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_LEGICDEMO_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       129
#define IDI_CARD                        129
#define IDC_STATIC_PORT                 1000
#define IDC_COMBO_COM                   1001
#define IDC_STATIC_READER               1002
#define IDC_STATIC_READER_TYPE          1003
#define IDC_EDIT_READER_TYPE            1004
#define IDC_STATIC_HARDWARE_REV         1005
#define IDC_EDIT_HARDWARE_REV           1006
#define IDC_STATIC_POWER                1007
#define IDC_EDIT_POWER                  1008
#define IDC_STATIC_BOOTLOADER           1009
#define IDC_EDIT_BOOTLOADER             1010
#define IDC_STATIC_FIRMWARE             1011
#define IDC_EDIT_FIRMWARE               1012
#define IDC_STATIC_CRYPTMODE            1013
#define IDC_EDIT_CRYPTMODE              1014
#define IDC_BTN_READER_INFO             1015
#define IDC_STATIC_CARD_INFO            1016
#define IDC_LIST_CARDS                  1017
#define IDC_BTN_SCAN_CARDS              1018
#define IDC_STATIC_READ_WRITE           1019
#define IDC_BTN_READ                    1020
#define IDC_BTN_WRITE                   1021
#define IDC_EDIT7                       1022
#define IDC_BTN_CONNECT                 1023
#define IDC_BTN_DISCONNECT              1024
#define IDC_EDIT_STATUS                 1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
