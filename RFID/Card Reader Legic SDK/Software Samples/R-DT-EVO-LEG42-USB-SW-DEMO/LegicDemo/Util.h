#pragma once

#include "Error.h"

typedef struct _ERROR_NAME
{
	TCHAR*	szError;
	EERROR	eError;
}ERROR_NAME, *PERROR_NAME;

class CUtil
{
public:
	CUtil();
	virtual ~CUtil();
	static UINT CalcCRC16(BYTE* pData);
	static UINT CalcCRC16(BYTE* pData, UINT nSize, UINT nStartVal, WORD wPolinom);
	static EERROR GetError(BYTE bError);
	static TCHAR* GetError(EERROR eError);
	static CString ByteToString(BYTE* pData, UINT nSize);
	static BYTE XOR(BYTE* pData, UINT nLength, BYTE bEndByte);
};

#define START_ERROR()	DWORD dwTime = GetTickCount()

#define END_ERROR(X)		CString strError = _T("");	\
							strError.Format(_T("%s: %s ! Exec time: %d (ms)"), X, CUtil::GetError(eRet), GetTickCount() - dwTime);	\
							m_ctrlStatus.SetWindowText(strError)