#include "twn4.sys.h"

#ifndef VERSION
  #define VERSION               0x0100
#endif
#ifndef APPCHARS
  #define APPCHARS              APP
#endif

#define APP_MAGIC_V1            0x49A31CF1
#define APP_MAGIC_V2            0x49A31CF2

const unsigned char AppManifest[] __attribute__((weak));

const unsigned char AppManifest[] =
{
    OPEN_PORTS, 1, OPEN_PORT_USB_MSK | OPEN_PORT_COM1_MSK | OPEN_PORT_COM2_MSK,
    TLV_END
};

typedef struct __attribute__((__packed__))
{
    unsigned long           Magic;
    unsigned long           Crc;
    unsigned long           Stack;
    unsigned long           ResetHandler;
    unsigned short          DeviceType;
    char                    AppChars[4];
    unsigned short          AppVersion;
    const unsigned char *   Manifest;
} TAppHeader;

// Data defined by the linker script
extern unsigned long _stext;
extern unsigned long _etext;
extern unsigned long _sdata;
extern unsigned long _edata;
extern unsigned long _sbss;
extern unsigned long _ebss;
extern unsigned long _estack;

void ResetHandler(void);
void _AppToolsInit(void);
int main(void);

#define STR_VALUE(arg)  STR_VALUE2(arg) 
#define STR_VALUE2(arg) #arg 

__attribute__ ((section(".header"), used))
const TAppHeader AppHeader = 
    { APP_MAGIC_V2, 0xFFFFFFFF, (unsigned long)&_estack, (unsigned long)ResetHandler, 128, STR_VALUE(APPCHARS), VERSION, AppManifest };

void ResetHandler (void)
{
    unsigned long *pSrc;
    unsigned long *pDest;
   
    pSrc  = &_etext;
    pDest = &_sdata;
    while(pDest < &_edata)
        *pDest++ = *pSrc++;
   
    pDest = &_sbss;
    while(pDest < &_ebss)
        *pDest++ = 0;

	_AppToolsInit();
	
    main();
    
    while (1)
        ;
}
