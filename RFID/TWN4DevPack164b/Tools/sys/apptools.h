#ifndef __APPTOOLS_H__
#define __APPTOOLS_H__

// ******************************************************************
// ****** Timer Functions *******************************************
// ******************************************************************

void StartTimer(unsigned long Duration);
void StopTimer(void);
bool TestTimer(void);

// ******************************************************************
// ****** General Communication *************************************
// ******************************************************************

bool TestByte(int Channel);
// byte ReadByte(int Channel); -> is System Function
bool TestChar(int Channel);
char ReadChar(int Channel);

// void WriteByte(int Channel,byte Byte); -> is System Function
void WriteChar(int Channel,char Char);
void WriteString(int Channel,const char *String);
void WriteRadix(int Channel,const byte *ID,int BitCnt,int DigitCnt,int Radix);
void WriteBin(int Channel,const byte *ID,int BitCnt,int DigitCnt);
void WriteDec(int Channel,const byte *ID,int BitCnt,int DigitCnt);
void WriteHex(int Channel,const byte *ID,int BitCnt,int DigitCnt);
void WriteVersion(int Channel);

// ******************************************************************
// ****** Host Communication ****************************************
// ******************************************************************

void SetHostChannel(int Channel);

bool HostTestByte(void);
byte HostReadByte(void);
bool HostTestChar(void);
char HostReadChar(void);

void HostWriteByte(byte Byte);
void HostWriteChar(char Char);
void HostWriteString(const char *String);
void HostWriteRadix(const byte *ID,int BitCnt,int DigitCnt,int Radix);
void HostWriteBin(const byte *ID,int BitCnt,int DigitCnt);
void HostWriteDec(const byte *ID,int BitCnt,int DigitCnt);
void HostWriteHex(const byte *ID,int BitCnt,int DigitCnt);
void HostWriteVersion(void);

// ******************************************************************
// ****** Beep Functions ********************************************
// ******************************************************************

void SetVolume(int NewVolume);
int GetVolume(void);
void BeepLow(void);
void BeepHigh(void);

// ******************************************************************
// ****** Compatibility to TWN3 *************************************
// ******************************************************************

int ConvertTagTypeToTWN3(int TagTypeTWN4);

#endif