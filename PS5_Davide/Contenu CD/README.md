# Projet de semestre 5

## Badges RFID connectés à l'iIot  

### Installation

Dans ce dossier, vous trouverez tous les fichiers utiles pour mettre en place le système embarqué (Raspberry Pi), le serveur OPC UA, et la lecture des badges avec le lecteur TWN4.

Pour installer le système, il faut au préalable avoir un utilisateur "pi" ou modifier, le cas échéant, le home directory dans le fichier rfid.service, et que le lecteur TWN4 soit connecté au Raspberry Pi.

Ensuite, il faut lancer le script script.sh depuis le dossier RFID. Celui-ci déplace les fichiers dans le home directory, puis nettoie le projet avant de faire un make et finalement copie le fichier rfid.service dans /etc/systemd/system/ pour pouvoir activer le service au prochain démarrage.

À la fin du script, le système redémarre et lance automatiquement le servuer OPC UA.

### Structure dossier RFID

Le dossier RFID est composé de la manière suivante :

    -les deux fichiers contenant les méthodes OPC UA pour le raspberry pi
        -open62541.h
        -open62541.c   

    -les deux fichiers pour la connection avec le lecteur RFID
        -rfid.h
        -rfid.c

    -les deux fichiers pour la création du servuer OPC UA
        -server_rfid.h
        -server_rfid.c

    -le fichier permettant de lancer le programme comme service au démarrage du raspberry pi
        -rfid.service

    -le Makefile qui va compiler le code pour le raspberry pi
        -Makefile
    
    -le script qui permet de tout automatiser
        -script.sh

    -deux dossiers contenant les fichiers OPC UA pour raspberry pi ou intel
        -open62541raspi
        -open62541intel
    



