/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Project:		HEIA-FR / Project 5
 *
 * Abstract:	Badge RFID connected to the iIoT
 *	
 * Purpose:		OPC UA server that send the UID read from the TWN4 to the client OPC UA
 *
 * Author:		Davide Previte / HEIA-FR
 * Date:		15.01.2019
 * 
 * 
 * Site where to find how to change the variable in the opc ua server
 * https://medium.com/gradiant-talks/implementing-an-opc-ua-server-using-open62541-77aeddd99370
 * 
 * Site where to find how to change the hostname 
 * https://github.com/open62541/open62541/issues/535
 * 
 */

#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include "open62541.h"
#include "server_rfid.h"
#include "rfid.h"

//Define the sampling time for the reader
#define SLEEP_TIME_MILLIS 50

//Define the ID of the node externally as it will be needed inside the thread
#define UID_NODE_ID 20305
#define CHECK_NODE_ID 21305

UA_String myUID; //variable that contains the uid for the OPC UA server

UA_Server *server; //it is the OPC UA server

//UA_String_init(&myUID); /* _init zeroes out the entire memory of the datatype */

/**
 * Method that stop the server if ctrl-c is catch
 * @param sig
 */
static void stopHandler(int sig)
{
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Received ctrl-c");
    running = false;
}

/**
 * Method that send the UID on the OPC UA server
 * @param uid is a String with the UID
 */
void sendUID(char *uid)
{
    UA_Variant value;
    myUID.length = strlen(uid);
    myUID.data = (UA_Byte *)uid;
    UA_Variant_setScalarCopy(&value, &myUID, &UA_TYPES[UA_TYPES_STRING]);
    UA_Server_writeValue(server, UA_NODEID_NUMERIC(1, UID_NODE_ID), value);
}

/**
 * Thread used to monitorize the sensor 
 * @param ptr is the pointer of the server OPC UA
 */
void *mainReader(void *ptr)
{
    server = ptr;                       //the pointer is the server
    int utime = SLEEP_TIME_MILLIS * 10; //time to sleep before sending next uid
    char uid[MAX_UID];                  //current uid read from the RFID reader
    char uid_old[MAX_UID];              //last uid read from the RFID reader
    int nbCheck = 0;                    //number of time that the same uid is read
    char msg[MAX_UID];                  //it is the String sended to the server that contains the UID and the number of time that it was read

    int fd = openSerial("/dev/ttyACM0"); //file descriptor of the RFID reader

    while (running == 1)
    {
        char *uid = readUID(fd); //UID read from the RFID reader

        //if the uid is the same that last uid, we should increment nbCheck
        if (strcmp(uid, uid_old) == 0)
        {
            nbCheck += 1;
            snprintf(msg, MAX_UID, "%s ; %d", uid, nbCheck); //create the String for the server
            sendUID(msg);

        }
        //else it is a new uid, so nbCheck is 1
        else{
            nbCheck = 1;
            snprintf(msg, MAX_UID, "%s ; %d", uid, nbCheck); //create the String for the server
            sendUID(msg);
            strcpy(uid_old, uid);
        }
        usleep(utime);
    }
}

/**
 * Method needed to add the UID variable on the server OPC UA
 * @param server is the pointer of the OPC UA server
 */
static void addUidVariable(UA_Server *server)
{
    UA_NodeId uidNodeId = UA_NODEID_NUMERIC(1, UID_NODE_ID);
    UA_QualifiedName uidName = UA_QUALIFIEDNAME(1, "UID");
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    attr.description = UA_LOCALIZEDTEXT("en_US", "UID number");
    attr.displayName = UA_LOCALIZEDTEXT("en_US", "UID");
    attr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    UA_Variant_setScalarCopy(&attr.value, &myUID, &UA_TYPES[UA_TYPES_STRING]);
    UA_Server_addVariableNode(server, uidNodeId,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                              UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                              uidName, UA_NODEID_NULL, attr, NULL, NULL);
}

/**
 * Main programm that launch the server OPC UA and add all the variable. Then it creates a thread that read the uid of the RFID reader
 * and send that to the server
 */
int main(void)
{
    int ret;
    pthread_t threadReader;
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);
    UA_ServerConfig *config = UA_ServerConfig_new_default();
    UA_Server *server = UA_Server_new(config);

    addUidVariable(server);

    //Launch the thread. The OPC-UA server is passed as parameter because the value of the node needs to be updated.
    if (pthread_create(&threadReader, NULL, mainReader, server))
    {
        fprintf(stderr, "Error - pthread_create(): %d\n", ret);
        exit(EXIT_FAILURE);
    }
    UA_StatusCode retval = UA_Server_run(server, &running); //the programm remains blocked here while any signal is catched
    UA_Server_delete(server);
    UA_ServerConfig_delete(config);
    return (int)retval;
}