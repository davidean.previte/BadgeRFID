#!bin/sh

#do all the update and upgrade to start with a clean installation
sudo apt update -y && sudo apt upgrade -y && sudo apt dist-upgrade -y

echo "Installing rfid reader..."

#copy all the file in the home directory
cp -r ../RFID $HOME

#go to the home directory
cd $HOME/RFID

#copy the rfid service into the systemd directory (should be sudo)
sudo cp rfid.service /etc/systemd/system/

#clean the project
make clean
#make the project
make

#enable the rfid service (should be done after making the project)
echo "Enabling rfid service"
sudo systemctl enable rfid
echo "Ok"

echo "Reboot the system to launch the OPC UA server with the reader attached"
sudo reboot



