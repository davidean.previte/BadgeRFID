/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Project:		HEIA-FR / Project 5
 *
 * Abstract:	Badge RFID connected to the iIoT
 *	
 * Purpose:		Read the UID from the TWN4 reader
 *
 * Author:		Davide Previte / HEIA-FR
 * Date:		15.01.2019
 * 
 */

#include "rfid.h"

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

char uidS[MAX_UID]; //char array that should contains the UID

/**
 * open the reader with termios structure
 * @param serial is the location of the reader ("/dev/ttyACM0" by default)
 * @return fd is the file desciptor of the RFID reader
 */
int openSerial(const char *serial)
{
    int fd = open(serial, O_RDWR | O_NOCTTY | O_NDELAY);

    if (fd == -1)
        printf("Error! in Opening RFID Reader \n");
    else
        printf("RFID Reader Opened Successfully \n");

    /**---------- Setting the Attributes of the serial port using termios structure --------- */

    struct termios SerialPortSettings; /* Create the structure                          */

    tcgetattr(fd, &SerialPortSettings); /* Get the current attributes of the Serial port */

    cfsetispeed(&SerialPortSettings, B115200); /* Set Read  Speed as 9600                       */
    cfsetospeed(&SerialPortSettings, B115200); /* Set Write Speed as 9600                       */

    SerialPortSettings.c_cflag &= ~PARENB; /* Disables the Parity Enable bit(PARENB),So No Parity   */
    SerialPortSettings.c_cflag &= ~CSTOPB; /* CSTOPB = 2 Stop bits,here it is cleared so 1 Stop bit */
    SerialPortSettings.c_cflag &= ~CSIZE;  /* Clears the mask for setting the data size             */
    SerialPortSettings.c_cflag |= CS8;     /* Set the data bits = 8                                 */

    SerialPortSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
    SerialPortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */

    SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);         /* Disable XON/XOFF flow control both i/p and o/p */
    SerialPortSettings.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG); /* Non Cannonical mode                            */

    SerialPortSettings.c_oflag &= ~OPOST; /*No Output Processing*/

    if ((tcsetattr(fd, TCSANOW, &SerialPortSettings)) != 0) /* Set the attributes to the termios structure*/
        printf("ERROR ! in Setting attributes \n");
    else
        printf("BaudRate = 115200 \n  StopBits = 1 \n  Parity   = none\n");

    return fd;
}

/**
 * read the uid and return it
 * @param fd is the file descriptor of the RFID reader
 * @return uidS char[] with the UID read by the RFID reader
 */
char* readUID(int fd)
{
    unsigned nbBytes = 0; //number of bytes read
    char x; //char that is read by the reader

    read(fd, &x, 1);

    //if the char read is one of this, nothing to do
    while(x == '\0' || x == '\n' || x == '\r') read(fd, &x, 1);

    //read the UID while there is a char 
    while(x != '\n' && x != '\r'){
        uidS[nbBytes++] = x;
        if (nbBytes  >= sizeof(uidS)) {
            printf("there is an error");
            return 0; // --> error!!!
        }
        read(fd, &x, 1);
    }

    //the last char should be a 0 because it is a string
    uidS[nbBytes] = 0;
    return uidS;
}

