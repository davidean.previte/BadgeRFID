#ifndef __PRS_H__
#define __PRS_H__

#define PRS_COMM_MODE_MSK		0x03
#define PRS_COMM_MODE_BINARY	0x00
#define PRS_COMM_MODE_ASCII		0x01

#define PRS_COMM_CRC_MSK		0x0C
#define PRS_COMM_CRC_OFF		0x00
#define PRS_COMM_CRC_ON			0x04

extern byte SimpleProtoMessage[17000];
extern int SimpleProtoMessageLength;

bool SimpleProtoInit(int Channel,int Mode);
bool SimpleProtoTestCommand(void);
void SimpleProtoExecuteCommand(void);
void SimpleProtoSendResponse(void);

#endif
