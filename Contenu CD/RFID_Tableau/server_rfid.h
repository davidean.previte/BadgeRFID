#pragma once
#ifndef SERVER_RFID_H
#define SERVER_RFID_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Project:		HEIA-FR / Project 5
 *
 * Abstract:	Badge RFID connected to the iIoT
 *	
 * Purpose:		OPC UA server that send the UID read from the TWN4 to the client OPC UA
 *
 * Author:		Davide Previte / HEIA-FR
 * Date:		15.01.2019
 * 
 * 
 * Site where to find how to change the variable in the opc ua server
 * https://medium.com/gradiant-talks/implementing-an-opc-ua-server-using-open62541-77aeddd99370
 * 
 * Site where to find how to change the hostname 
 * https://github.com/open62541/open62541/issues/535
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include "open62541.h"
#include "rfid.h"
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>

//Define the sampling time for the sensor
#define SLEEP_TIME_MILLIS 50


UA_Boolean running = true; // Boolean that show if the server is running or not

/**
 * Method that stop the server if ctrl-c is catch
 * @param sig
 */
static void stopHandler(int sig);

/**
 * Method that send the UID on the OPC UA server
 * @param uid is a String with the UID
 * @param username
 */
void sendUID(char* uid, char* username);

/**
 * Thread used to monitorize the sensor 
 * @param ptr is the pointer of the server OPC UA
 */
void *mainSensor(void *ptr);

/**
 * Method needed to add the UID variable on the server OPC UA
 * @param server is the pointer of the OPC UA server
 */
static void addUidVariable(UA_Server *server);

/**
 * Method needed to add the User variable on the server OPC UA
 * @param server is the pointer of the OPC UA server
 */
static void addUserVariable(UA_Server *server);

/**
 * Main programm that launch the server OPC UA and add all the variable. Then it creates a thread that read the uid of the RFID reader
 * and send that to the server
 */
int main(void);

#endif