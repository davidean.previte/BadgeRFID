#pragma once
#ifndef RFID_H
#define RFID_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Project 5
 *
 * Abstract:	Badge RFID connected to the iIoT
 *	
 * Purpose:		Header for reading the UID from the TWN4 reader
 *
 * Author:		Davide Previte / HEIA-FR
 * Date:		15.01.2019
 */

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>


//Warning : must be big enough !!
#define MAX_UID 50

/**
 * open the reader with termios structure
 * @param serial is the location of the reader ("/dev/ttyACM0" by default)
 * @return fd is the file desciptor of the RFID reader
 */
int openSerial(const char *serial);

/**
 * read the uid and return it
 * @param fd is the file descriptor of the RFID reader
 * @return uidS char[] with the UID read by the RFID reader
 */
char* readUID(int fd);


#endif
